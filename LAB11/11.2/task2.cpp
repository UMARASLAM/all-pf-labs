#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <iomanip>
using namespace std;
int main()
{
	fstream file1("file1.txt", ios::in| ios::out);
	char edit = 'y';
	while(edit=='y')
	{
	cout<<"Enter line number : ";
	int num; 
	cin>>num;
	num = (num-1)*60;
	file1.seekg(num, ios::beg);
	string line;
	getline(file1, line);
	cout<<line<<endl;
	cout<<"Which data type you want to edit"<<endl;
	cout<<"'r' for rollno"<<endl<<"'n' for name"<<endl<<"'c' for crs"<<endl;
	cout<<"'m' for mids"<<endl<<"'s' for sessionals"<<endl<<"'f' for finals"<<endl;
	file1.seekp(-60, ios::cur);
	char dtype;
	cin>>dtype;
	if(dtype=='r')
	{
		file1.seekp(0, ios::cur);
		cout<<"Enter roll number of 10 characters : ";
		string rollno;
		cin>>rollno;
		file1<<setw(10)<<rollno;
	}
	
	if(dtype=='n')
	{
		file1.seekp(10, ios::cur);
		cout<<"Enter Name (Maximum of 30 characters) : ";
		string name;
		cin.ignore(1);
		getline(cin, name, '\n');
		file1<<left<<setw(30)<<name;
	}
	
	if(dtype=='c')
	{
		file1.seekp(40, ios::cur);
		cout<<"Enter CRS (Maximum of 10 characters) : ";
		string crs;
		cin>>crs;
		file1<<left<<setw(10)<<crs;
	}
	
	if(dtype=='m')
	{
		file1.seekp(50, ios::cur);
		cout<<"Enter Mids marks (Maximum of 2 characters) : ";
		int mids;
		cin>>mids;
		file1<<setw(2)<<mids;
	}
	
	if(dtype=='s')
	{
		file1.seekp(53, ios::cur);
		cout<<"Enter sessionals marks (Maximum of 2 characters) : ";
		int sessional;
		cin>>sessional;
		file1<<setw(2)<<sessional;
	}
	
	if(dtype=='f')
	{
		file1.seekp(56, ios::cur);
		cout<<"Enter finals marks (Maximum of 10 characters) : ";
		int finals;
		cin>>finals;
		file1<<setw(2)<<finals;
	}
	cout<<"Do you want to edit more data (y/n) :";
	cin>>edit;
	}
	cout<<"Your file is updated successfully";
	file1.close();
	return 0;
}