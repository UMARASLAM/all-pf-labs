#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
using namespace std;
int main()
{
	fstream file1("file0.txt", ios::in);
	if (!file1.is_open())
	{
		cout<<"Sorry, Required File is missing to open";
		return 0;
	}
	cout<<"Do you want to store errors in another file (y/n) :";
	char store; cin>>store;
	string filename;
	if (store == 'y')
	{
		cout<<"Enter File Name : ";
		cin.ignore(1);
		getline (cin, filename);
	}
		fstream file2(filename.c_str(), ios::out);
	int record=0; string line[1000]; int loop=0;
	while(!file1.eof())
	{
		char rollno[100]; char name[100]; char subject[100]; char mark1[100];char mark2[100]; char mark3[100];
		record = record+1;
		file1.getline(rollno, 11);
		file1.clear();
		for (int i=0; i<10; i++)
		{
		if(rollno[i] == ' ')
			{
				cout<<"Error on line "<<record<<". Incomplete roll number"<<endl;
				if (store== 'y')
				{
				file2<<"Error on line "<<record<<". Incomplete roll number"<<endl;
				}
				break;
			}
		}
		file1.getline(name, 31);
		file1.clear();
		file1.getline(subject, 11);
		file1.clear();
		if(subject[0]==' '&&subject[1]==' '&&subject[2]==' ')
		{
			cout<<"Error on line "<<record<<". CRS Missing"<<endl;
			if (store== 'y')
				{
				file2<<"Error on line "<<record<<". CRS Missing"<<endl;
				}
		}
		file1.getline(mark1, 4);
		file1.clear();
			if((mark1[0]<'0'||mark1[0]>'9')||(mark1[1]<'0'||mark1[1]>'9'))
			{
				cout<<"Error on line "<<record<<". Inapproprite Mids Marks"<<endl;
				if (store== 'y')
				{
				file2<<"Error on line "<<record<<". Inapproprite Mids Marks"<<endl;
				}
			}
		file1.getline(mark2, 4);
		file1.clear();
			if((mark2[0]<'0'||mark2[0]>'9')||(mark2[1]<'0'||mark2[1]>'9'))
			{
				cout<<"Error on line "<<record<<". Inapproprite Sessional Marks"<<endl;
				if (store== 'y')
				{
				file2<<"Error on line "<<record<<". Inapproprite Sessional Marks"<<endl;
				}
			}
		file1.getline(mark3, 3);
			if((mark3[0]<'0'||mark3[0]>'9')||(mark3[1]<'0'||mark3[1]>'9'))
			{
				cout<<"Error on line "<<record<<". Inapproprite Final Marks"<<endl;
				if (store== 'y')
				{
				file2<<"Error on line "<<record<<". Inapproprite Final Marks"<<endl;
				}
			}
		file1.seekg(-60, ios::cur);
		file1>>line[loop];
		if (line[loop]==line[loop-1]&&line[loop-1]==line[loop-2]&&line[loop-2]==line[loop-3])
		{
			cout<<"Error on line "<<record<<". A student can enroll in maximum of three courses"<<endl;
			if (store== 'y')
				{
				file2<<"Error on line "<<record<<". A student can enroll in maximum of three courses"<<endl;
				}
		}
		loop= loop+1;
		file1.ignore(100, '\n');
	}
	if (store=='y')
	{
		cout<<endl<<endl<<"Errors are stored in "<<filename<<" successfully";
	}
	file1.close();
	file2.close();
	return 0;
}
