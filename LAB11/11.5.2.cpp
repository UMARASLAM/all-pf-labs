#include<iostream>
#include<fstream>
#include<string>
using namespace std;

struct Date { int day,month,year; } ;

void alphaDate(int day,int month,int year)
{
	int wse;
	string Month[13];
	string m = "Invalid";
	switch(month)
	{
		case 1:
			Month[1] = "January";
			break;
		case 2:
			Month[2] = "February";
			break;
		case 3:
			Month[3] = "March";
			break;
		case 4:
			Month[4] = "April";
			break;
		case 5:
			Month[5] = "May";
			break;
		case 6:
			Month[6] = "June";
			break;
		case 7:
			Month[7] = "July";
			break;
		case 8:
			Month[8] = "August";
			break;
		case 9:
			Month[9] = "September";
			break;
		case 10:
			Month[10] = "October";
			break;
		case 11:
			Month[11] = "November";
			break;
		case 12:
			Month[12] = "December";
			break;
		default:
			break;
	}

	if((month==1&&day>31) || (month==2&&day>29&&year%4==0) || (month==2&&day>28&&year%4!=0) || (month==3&&day>31) || (month==4&&day>30) || (month==5&&day>31) || (month==6&&day>30) || (month==7&&day>31) || (month==8&&day>31) || (month==9&&day>30) || (month==10&&day>31) || (month==11&&day>30) || (month==12&&day>31) || (month>12))
	{
		throw m;
	}
	else
	cout << Month[month] << " " << day << ", " << year;
}

int main()
{
	string m = "Invalid";
	Date input;
	cout<<"Enter day ";
	cin >> input.day;
	cout<<"Enter Month ";
	cin >> input.month;
	cout<<"Enter year ";
	cin >> input.year;
	try
	{
		alphaDate(input.day,input.month,input.year);
	} catch(string m)
	{
		cout << "Days are invalid for the month of "<<input.month<<endl;
        cout<<"Month value can be within 1 to 12";
	}


	cout << endl;

	return 0;
}

