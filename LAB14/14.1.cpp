#include<iostream>

using namespace std;

int main()
{
    int val1=2300,val2;
    //a
    cout<<"val1 address is "<<&val1<<endl<<"val2 address is "<<&val2<<endl;
    //b
    int *myPointer;
    //c
    cout<<sizeof(*myPointer)<<endl;
    //d
    myPointer=&val1;
    //e
    cout<<*myPointer<<endl;
    //f
    *myPointer=val2;
    //g
    cout<<val2<<endl;
    //h
    cout<<myPointer<<endl;
    //i
    cout<<*myPointer<<endl;
    //j
    *myPointer=NULL;
    cout<<*myPointer;
	return 0;
}
