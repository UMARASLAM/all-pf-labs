#include <iostream>
#include <algorithm>

using namespace std;
void show(int a[],int length);
float AVG(int a[],int length);
int main()
{
    int length;
    cout<<"Enter the length ";
    cin>>length;
     int *ps=new int [length],n;
    cout<<"Enter "<<length<<" different scores"<<endl;
    for(int i=0;i<length;i++)
    {
        cin>>n;
     *(ps+i)=n;
    }
   sort(ps, ps+length);
    cout << "\n\n The array after sorting is : ";
    show(ps,length);
    cout<<"\n\n Average is "<<AVG(ps,length);
   delete[] ps;
    return 0;

}

void show(int a[],int length)
{
    for(int i = 0; i < length; ++i)
        cout << a[i] << ",";


}
float AVG(int a[],int length)
{
    int sum=0;
    float AVG;
    for(int i = 0; i < length; ++i)
     sum=sum+a[i];
    AVG=sum/length;
    return AVG;
}
