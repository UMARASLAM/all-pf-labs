#include<iostream>

using namespace std;

int main()
{

	float x1, y1;
	float x2, y2;
	float xr, yr;
	
	cout << "Enter x and y components of a vector one by one" << endl;
	cin >> x1;
	cin >> y1;
	
	cout << "Enter x and y components of another vector one by one" << endl;
	cin >> x2;
	cin >> y2;
	
	xr = x1+x2;
	yr = y1+y2;
	
	cout << "Resultant of above vectors is ";
	cout << xr;
	cout << ',';
	cout << yr;
	
	return 0;
}
