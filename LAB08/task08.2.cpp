
#include<iostream>
using namespace std;
double diff(double n,double mid);
int cubicRoot(int n);


int main()
{
    int n ;
    cout<<"Enter a number ";
    cin>>n;
    cout<<"Cubic root of " << n<<" is "<<cubicRoot(n);
    return 0;
}

int cubicRoot(int n)
{
    double start = 0, end = n;

      double e = 0.0000001;

    while (true)
    {
        double mid = (start + end)/2;
        double error = diff(n, mid);

          if (error <= e)
            return mid;
        if ((mid*mid*mid) > n)
            end = mid;

        else
            start = mid;
    }
}


double diff(double n,double mid)
{
    if (n > (mid*mid*mid))
        return (n-(mid*mid*mid));
    else
        return ((mid*mid*mid) - n);
}
