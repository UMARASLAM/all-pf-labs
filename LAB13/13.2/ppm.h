#ifndef __PPM_
#define __PPM__
#include<string>


struct AsciiPPM {
    char header[2];
    int width;
    int height;
    int shades;
    unsigned char R[300][300];
    unsigned char G[300][300];
    unsigned char B[300][300];
};
void fileRead(std::string fileName ,AsciiPPM & pic );
void fileWrite(std::string fileName,const AsciiPPM & pic);
void ignoreComment(std::ifstream & file) ;


 #endif




