#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>

using namespace std;

void ignoreComment(ifstream & file) {
    char c;
    while (true) {
        file >> ws;
        c = file.peek();
        if (c >= '0' && c <= '9') {
            return;
        } else if (c == '#') {
            file.ignore(4096, '\n');
        } else {
            file.ignore(1);
        }
    }
}

struct AsciiPPM {
    char header[2];
    int width;
    int height;
    int shades;
    unsigned char R[300][300]; // max image size is 300 X 300
    unsigned char G[300][300]; // max image size is 90000
    unsigned char B[300][300]; // max image size is 100 X 500 < = 90000
};

void fileRead(string fileName, AsciiPPM & pic) {
    ifstream img(fileName.c_str());
    if (!img.is_open()) {
        cout << "Error reading file: " << fileName << endl;
        throw 1;
    }

    //read header
    img >> pic.header[0];
    img >> pic.header[1];
    ignoreComment(img);
    img >> pic.width;
    ignoreComment(img);
    img >> pic.height;
    ignoreComment(img);
    img >> pic.shades;
    ignoreComment(img);

    //loop and read data
    int pval;
    for (int i = 0; i < pic.height; i++) {
        for (int j = 0; j < pic.width; j++) {
            img >> pval;
            pic.R[i][j] = pval;
            img >> pval;
            pic.G[i][j] = pval;
            img >> pval;
            pic.B[i][j] = pval;
        }
    }

    img.close();
}


int main() {
    cout << "Program placing an image and its flipped form togther" << endl;
    cout << "This may take some time, please wait . . ." << endl;

    AsciiPPM BKA;
    fileRead("brian_kernighan_ascii.ppm", BKA);
    ofstream write("brian_kernighan_new.ppm");


    AsciiPPM newBKA = BKA;
    for (long i = BKA.height-1 ;  i >=0 ; i--) {
        for (long j = BKA.width-1 ;  j >=0 ; j--) {
            // Image flip horizontly
            newBKA.R[i][BKA.width - j - 1] = BKA.R[i][j];
            newBKA.G[i][BKA.width - j - 1] = BKA.G[i][j];
            newBKA.B[i][BKA.width - j - 1] = BKA.B[i][j];


		}
    }


    write << BKA.header[0] << BKA.header[1] << endl;
    write << 2*BKA.width << endl;
    write << BKA.height << endl;
    write << BKA.shades << endl;

    for (long i =0  ;  i <BKA.height ; i++) {
        for (long j =0 ;  j <BKA.width ; j++) {
				if (i > 0 && i % 15 == 0)
                write << endl;

				write << int(BKA.R[i][j]) << " ";
				write << int(BKA.G[i][j]) << " ";
				write << int(BKA.B[i][j]) << " ";

		}

		for (long k =0 ;  k <BKA.width ; k++) {
				if (i > 0 && i % 15 == 0)
                write << endl;

				write << int(newBKA.R[i][k]) << " ";
				write << int(newBKA.G[i][k]) << " ";
				write << int(newBKA.B[i][k]) << " ";

		}

	}

	write.close();
    cout << "Task completed, thanks" << endl;

    return EXIT_SUCCESS;
}
