#include <iostream>
#include <fstream>


using namespace std;

int main()
{   ifstream ifile;
    char c;
    int characters = 0,lines = 0,alphabets = 0,vowels = 0,special_char = 0,words = 0;
    ifile.open("poem01.txt");
    if (!ifile)
     cout << "error";
    else
    {
      ifile.get(c);
    while(!ifile.eof())
    {

        if ( c >= 0 && c <= 255 )
         characters++;
        if ( c == '\n' )
         lines++;
        if ( c == ' ' || c == '\n' )
         words++;
        if ( c >= 'A' && c <='z'||c>='a'&&c<='z' )
        {
         alphabets++;
        if ( c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' || c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U')
         vowels++;
        }
        if ( c >= 32 &&  c <= 47 || c >= 58 && c <= 64 || c >= 91 && c <= 96 || c >= 123 && c <= 126 )
         special_char++;

        ifile.get(c);

    }
    }
cout << "Filename    : poem01.txt " << endl<<endl<<endl;
cout << "Lines       :        " << lines+1 << endl;
cout << "Character   :        " << characters << endl;
cout << "Vowels      :        " << vowels << endl;
cout << "Words       :        " << words << endl;
cout << "Alphabets   :        " << alphabets << endl;
cout << "Special char:        " << special_char<<endl;

    ifile.close();

return 0; }
