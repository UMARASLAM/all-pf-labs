#include<iostream>

using namespace std;

struct vector
{
	float x;
	float y;
	float z;
};

int main()
{

	vector v1,v2,vr;
	
	cout << "Enter x ,y and z components of a vector one by one" << endl;
	cin >> v1.x;
	cin >> v1.y;
	cin >> v1.z;
	
	cout << "Enter x ,y and z components of another vector one by one" << endl;
	cin >> v2.x;
	cin >> v2.y;
	cin >> v2.z;
	
	vr.x = v1.x+v2.x;
	vr.y = v1.y+v2.y;
	vr.z = v1.z+v2.z;
	cout << "Resultant of above vectors is ";
	cout << vr.x;
	cout << ',';
	cout << vr.y;
	cout<<","<<vr.z;
	
	
	return 0;
}
